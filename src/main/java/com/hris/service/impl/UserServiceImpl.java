package com.hris.service.impl;

import com.hris.dto.GeneralResponse;
import com.hris.dto.RequestRegister;
import com.hris.model.User;
import com.hris.repository.UserRepository;
import com.hris.service.UserService;
import com.hris.utils.Helper;
import com.hris.utils.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author "Noverry Ambo"
 * @start 3/24/2024
 */

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public GeneralResponse doRegister(RequestRegister requestRegister) {

//        String currentUser = doGetUsername();
        String dateOfBirth = Helper.sdf.format(requestRegister.getDateOfBirth());
        Optional<User> reference = userRepository.findByUsernameOrEmail(requestRegister.getUsername(), requestRegister.getUsername());
        if (reference.isPresent()) {
            return GeneralResponse.builder()
                    .responseCode(ResponseCode.DUPLICATE_USER.getCode())
                    .responseMessage(ResponseCode.DUPLICATE_USER.getCode())
                    .build();
        }else{
            User user = new User();
            user.setUsername(requestRegister.getUsername());
            user.setPassword(BCrypt.hashpw(requestRegister.getPassword(), BCrypt.gensalt()));
            user.setEmail(requestRegister.getEmail());
            user.setProfileName(requestRegister.getProfileName());
            user.setDateOfBirth(dateOfBirth);
            user.setNoRekening(requestRegister.getNoRekening());
            user.setDomicileAddress(requestRegister.getDomicileAddress());
            user.setPendidikanTerakhir(requestRegister.getPendidikanTerakhir());
            user.setMartialStatus(requestRegister.getMartialStatus());
            user.setNamaIbu(requestRegister.getNamaIbu());
            user.setNoBpjsKesehatan(requestRegister.getNoBpjsKesehatan());
            user.setNoBpjsKetenagakerjaan(requestRegister.getNoBpjsKetenagakerjaan());
            user.setNoKtp(requestRegister.getNoKtp());
            user.setNpwp(requestRegister.getNpwp());
            user.setPhone(requestRegister.getPhone());
            user.setPlaceOfBirth(requestRegister.getPlaceOfBirth());
            user.setReligion(requestRegister.getReligion());
            user.setResidenceAddress(requestRegister.getResidenceAddress());
            userRepository.save(user);

            return GeneralResponse.builder()
                    .responseCode(ResponseCode.SUCCESS.getCode())
                    .responseMessage(ResponseCode.SUCCESS.getMessage())
                    .data(user)
                    .build();
        }
    }

    @Override
    public boolean existByUsername(String username) {
        Boolean isEksist = userRepository.existsByUsername(username);
        return isEksist;
    }

    private String doGetUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String user = authentication.getName();
        return user;
    }
}
