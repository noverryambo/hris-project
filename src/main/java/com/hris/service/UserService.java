package com.hris.service;

import com.hris.dto.GeneralResponse;
import com.hris.dto.RequestRegister;

/**
 * @author "Noverry Ambo"
 * @start 3/24/2024
 */
public interface UserService {

    GeneralResponse doRegister(RequestRegister requestRegister);

    boolean existByUsername(String username);
}
