package com.hris.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hris.dto.GeneralResponse;
import com.hris.utils.ResponseCode;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author "Noverry Ambo"
 * @start 3/24/2024
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        GeneralResponse<String> unauthorizedResponse = GeneralResponse.<String>builder()
                .responseCode(ResponseCode.UNAUTHORIZE.getCode())
                .responseMessage(ResponseCode.UNAUTHORIZE.getMessage())
                .build();

        objectMapper.writeValue(response.getOutputStream(), unauthorizedResponse);
    }
}
