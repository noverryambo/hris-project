package com.hris.utils;

/**
 * @author "Noverry Ambo"
 * @start 3/23/2024
 */
public enum ResponseCode {

    SUCCESS("00", "Successfull"),
    DUPLICATE_USER("40", "Username Already Register"),
    UNAUTHORIZE("41", "Unauthorized"),
    INVALID_TOKEN("42", "Invalid Token"),
    RESOURCE_NOT_FOUND("44", "Resource Not Found");

    private String code;
    private String message;
    ResponseCode(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
