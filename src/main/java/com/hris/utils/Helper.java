package com.hris.utils;

import java.text.SimpleDateFormat;

/**
 * @author "Noverry Ambo"
 * @start 4/6/2024
 */
public class Helper {

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
