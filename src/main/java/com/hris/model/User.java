package com.hris.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @author "Noverry Ambo"
 * @start 3/23/2024
 */

@Data
@Builder
@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "username"))
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity{

    public enum Role{
        ROLE_USER,
        ROLE_ADMIN
    }

    @Column(name = "no_rekening")
    private String noRekening;

    @Column(name = "active")
    private boolean active;

    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Column(name = "domicile_address")
    private String domicileAddress;

    @Column(name = "pendidikan_terakhir")
    private String pendidikanTerakhir;

    @Column(name = "martial_status")
    private String martialStatus;

    @Column(name = "nama_ibu")
    private String namaIbu;

    @Column(name = "nick_name")
    private String nickName;

    @Column(name = "no_bpjs_ketenagakerjaan")
    private String noBpjsKetenagakerjaan;

    @Column(name = "no_bpjs_kesehatan")
    private String noBpjsKesehatan;

    @Column(name = "no_ktp")
    private String noKtp;

    @Column(name = "npwp")
    private String npwp;

    @Column(name = "password")
    @JsonIgnore
    private String password;

    @Column(name = "phone")
    private String phone;

    @Column(name = "place_of_birth")
    private String placeOfBirth;

    @Column(name = "profile_name")
    private String profileName;

    @Column(name = "religion")
    private String religion;

    @Column(name = "residence_address")
    private String residenceAddress;

    @Column(name = "username", columnDefinition = "VARCHAR(50)", nullable = false, unique = true)
    private String username;

    @Column(name = "email", columnDefinition = "VARCHAR(50)", nullable = false)
    private String email;

    @Column(name = "token")
    private String token;

    @Column(name = "role", columnDefinition = "VARCHAR(50)")
    @Enumerated(EnumType.STRING)
    private Role role = Role.ROLE_USER;
}
