package com.hris.exception;

import com.google.gson.Gson;
import com.hris.dto.GeneralResponse;
import com.hris.utils.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * @author "Noverry Ambo"
 * @start 3/23/2024
 */

@ControllerAdvice
@Slf4j
public class GeneralExceptionHandler extends ResponseEntityExceptionHandler {

    private Gson gson = new Gson();

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseEntity<?> runtimeException(Exception ex, HttpServletRequest request){
        log.error(ex.getMessage());
        GeneralResponse response = null;
        if (ex instanceof UnauthorizedException){
            response = GeneralResponse.builder()
                    .responseCode(ResponseCode.UNAUTHORIZE.getCode())
                    .responseMessage(ResponseCode.UNAUTHORIZE.getMessage())
                    .data(ex.getMessage())
                    .build();
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        } else if (ex instanceof InvalidTokenException){
            response = GeneralResponse.builder()
                    .responseCode(ResponseCode.INVALID_TOKEN.getCode())
                    .responseMessage(ResponseCode.INVALID_TOKEN.getMessage())
                    .data(ex.getCause())
                    .build();
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        }

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//    @ExceptionHandler(InvalidTokenException.class)
//    public ResponseEntity<Object> handleInvalidTokenException(InvalidTokenException ex, HttpServletRequest request) {
//        GeneralResponse response = GeneralResponse.builder()
//                .responseCode("01")
//                .responseMessage(ex.getMessage())
//                .build();
//        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
//    }
}
