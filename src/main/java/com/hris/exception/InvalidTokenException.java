package com.hris.exception;

import lombok.Getter;

@Getter
public class InvalidTokenException extends RuntimeException {

    private int status = 401;
    private Object data;

    public InvalidTokenException() {
        super("Invalid Token");
    }

    public InvalidTokenException(String message){
        super(message);
    }

    public InvalidTokenException(String message, Object data){
        super(message);
        this.data = data;
    }

    public InvalidTokenException(String message, int status, Object data){
        super(message);
        this.status = status;
        this.data = data;
    }

    public static InvalidTokenException create(String message) {
        return new InvalidTokenException(message);
    }

    public static InvalidTokenException create(String message, int status) {
        return new InvalidTokenException(message, status);
    }

    public static InvalidTokenException create(String message, int status, Object data) {
        return new InvalidTokenException(message, status, data);
    }


}
