package com.hris.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestRegister {

    private String npwp;
    private String phone;
    private String placeOfBirth;
    private String religion;
    private String residenceAddress;
    private String pendidikanTerakhir;
    private String martialStatus;
    private String namaIbu;
    private String noBpjsKetenagakerjaan;
    private String noBpjsKesehatan;
    private String noKtp;
    private String dateOfBirth;
    private String domicileAddress;
    private String noRekening;
    private String username;
    private String password;
    private String email;
    private String profileName;
}
