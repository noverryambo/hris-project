package com.hris;

import com.hris.model.User;
import com.hris.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author "Noverry Ambo"
 * @start 3/24/2024
 */

@SpringBootTest
public class InsertUserTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void addUser(){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date birthDate = sdf.parse("1987-08-17");
            User user = User.builder()
                    .role(User.Role.ROLE_ADMIN)
                    .noRekening("0000000001")
                    .active(true)
                    .dateOfBirth("1987-08-17")
                    .domicileAddress("Jogja")
                    .pendidikanTerakhir("Saryana")
                    .martialStatus("pingin kawin")
                    .namaIbu("ibu pertiwi")
                    .nickName("BambangXYZ")
                    .noBpjsKetenagakerjaan("00000000001")
                    .noBpjsKesehatan("00000000001")
                    .noKtp("3471130711870001")
                    .npwp("34564-556165-0000-6000")
                    .password("jembatan")
                    .phone("08752135462")
                    .placeOfBirth("Djogjah")
                    .profileName("bambang xyz")
                    .religion("rahasia")
                    .residenceAddress("Boghor")
                    .username("bambang_xyz")
                    .build();
            userRepository.save(user);
        }catch (Exception e){
            e.getMessage();
        }
    }
}
